package HomepageMethods;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

import HomepageObjects.testcases;

public class Methodcases {
	
	@Test
	public void nextStep() throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "/home/delta/Documents/eclipse/chromedriver_linux64/chromedriver");
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.propertycapsule.com/");

		
		
		testcases test = new testcases(driver);
		
		test.rqstaDemoMethod();
		Thread.sleep(5000);
		test.ClosebtnMethod();
		
		Thread.sleep(5000);
	
		
		test.contactUsMethod();
		Thread.sleep(5000);
		test.ClosebtnMethod();
		
		test.homeBtn();
		Thread.sleep(5000);
		
		
		driver.quit();
	}

}
