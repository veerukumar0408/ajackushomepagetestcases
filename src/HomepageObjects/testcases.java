package HomepageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class testcases {
	
	public static WebDriver driver;
	
	
	
	By requestaDemo = By.xpath("/html/body/section[2]/div/div[2]/a[2]");
	By contactUs = By.xpath("/html/body/footer/ul/li[3]/a");
	By CloseBtn = By.xpath("//*[@id=\"contact-us-form-container\"]/i");
	By HomepageBtn = By.xpath("/html/body/header/div[1]/a/img");
	
	

	public static  void scroll(By elements) {
		JavascriptExecutor j = (JavascriptExecutor) driver;
		WebElement element = driver.findElement(elements);
		j.executeScript("arguments[0].scrollIntoView(true);", element);
	}
	
	public testcases(WebDriver driver) {
		this.driver = driver;
	}

	//testcase TC_10
	public void rqstaDemoMethod() throws InterruptedException {
		driver.findElement(requestaDemo).click();
	}
	
	//testcase TC_07
	public void contactUsMethod() throws InterruptedException {
		testcases.scroll(contactUs);
		Thread.sleep(5000);
		driver.findElement(contactUs).click();
	}
	public void ClosebtnMethod() {
		driver.findElement(CloseBtn).click();
	}
	
	
	//Testcase TC_05
	public void homeBtn() {
		driver.findElement(HomepageBtn).click();
	}

}
